function teste1(num) {
    if (num > 7) // O if sem chaves vai considerar apenas a primeira sentença após o if
        console.log(num)
        console.log('Final')
}

teste1(6)
teste1(8)

function teste2(num) {
    if (num > 7); { // cuidado com o ;, não usar com as estruras de controle
        console.log(num)
    }
}

teste2(6)
teste2(8)