function criarProduto(nome, preco) {
    return {
        nome,
        preco,
        desconto: 0.1
    }
}

console.log(criarProduto('Pneu', 100.5))
console.log(criarProduto('Oleo', 50.43))